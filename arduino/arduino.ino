int ledRed = 3;
int ledGreen = 5;
int pinBuzzer = 10;
int pinExtra = 7; // Pin adicional

unsigned long pinExraStartTime = 0; // Guarda el momento en que se activa el pin
const long pinExtraDuration = 5000; // Duración de la activación del pin en milisegundos
bool isPinExtraActive = false; // Indica si el pin está activo

void setup() {
  pinMode(ledRed, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  pinMode(pinBuzzer, OUTPUT);
  pinMode(pinExtra, OUTPUT);
  Serial.begin(9600);
  setYellowColor();
}

void loop() {
  if (Serial.available() > 0) {
    char receivedChar = Serial.read();
    if (receivedChar == 'c') {
      setGreenColor();
      correctSound();
      activatePinExtra();
      setYellowColor();
    } else if (receivedChar == 'i') {
      setRedColor();
      incorrectSound();
      setYellowColor();
    }
  }

  // Verifica si el pin está activo y si ha pasado el tiempo de duración
  if (isPinExtraActive && millis() - pinExtraStartTime >= pinExtraDuration) {
    digitalWrite(pinExtra, LOW); // Desactiva el pin
    isPinExtraActive = false;
  }
}

void correctSound() {
  tone(pinBuzzer, 2000);
  delay(500);
  noTone(pinBuzzer);
}

void incorrectSound() {
  tone(pinBuzzer, 500);
  delay(250); 
  noTone(pinBuzzer); 
  delay(100); 
  tone(pinBuzzer, 500); 
  delay(250); 
  noTone(pinBuzzer); 
}

void setYellowColor() {
  analogWrite(ledRed, 100);
  analogWrite(ledGreen, 100);
}

void setGreenColor() {
  analogWrite(ledRed, 0);
  analogWrite(ledGreen, 255);
}

void setRedColor() {
  analogWrite(ledRed, 255);
  analogWrite(ledGreen, 0);
}

void activatePinExtra() {
  if (!isPinExtraActive) {
    digitalWrite(pinExtra, HIGH); // Activa el pin
    pinExtraStartTime = millis(); // Guarda el momento de activación
    isPinExtraActive = true;
  }
}
