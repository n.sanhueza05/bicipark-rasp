import cv2
from pyzbar.pyzbar import decode
import time
import requests
import json
import serial

TIEMPO_ESPERA = 10
URL_API = "https://bicipark-api.nicolasrivas.me/park/activate/"

# Inicializar comunicación serial con Arduino
arduino = serial.Serial(port='COM3', baudrate=9600, timeout=.1)  # Asegúrate de cambiar 'COM3' al puerto correcto

def enviar_solicitud_put(park_id, user_id):
    url = f"{URL_API}{park_id}"
    headers = {
        'Content-Type': 'application/json'
    }
    data = json.dumps({'userId': user_id})

    try:
        response = requests.put(url, headers=headers, data=data)
        print(f"Realizando solicitud PUT a {url} con datos: {data}")
        if response.status_code == 200:
            print(f"Solicitud PUT exitosa para parkId {park_id}. Respuesta: {response.text}")
            arduino.write(b'c')  # Enviar 'c' al Arduino
        else:
            print(f"Error en la solicitud PUT para parkId {park_id}: {response.status_code}, Respuesta: {response.text}")
            arduino.write(b'i')  # Enviar 'i' al Arduino
    except requests.RequestException as e:
        print(f"Excepción al realizar solicitud PUT: {e}")
        arduino.write(b'i')  # Enviar 'i' al Arduino

def leer_codigo_qr(frame, ultimo_codigo, ultimo_tiempo_lectura):
    codigos = decode(frame)
    tiempo_actual = time.time()
    codigo_actual = None

    if codigos:
        codigo_actual = codigos[0].data.decode('utf-8')
        if codigo_actual != ultimo_codigo:
            try:
                print(f"Datos QR: {codigo_actual}")
                data = json.loads(codigo_actual)
                if "parkId" in data and "userId" in data:
                    enviar_solicitud_put(data["parkId"], data["userId"])
            except json.JSONDecodeError:
                print("Error al decodificar JSON del QR.")
            ultimo_codigo = codigo_actual
            ultimo_tiempo_lectura = tiempo_actual
    else:
        if ultimo_codigo is not None and tiempo_actual - ultimo_tiempo_lectura > TIEMPO_ESPERA:
            print("Código liberado.")
            ultimo_codigo = None

    return ultimo_codigo, ultimo_tiempo_lectura

def main():
    print("Iniciado.")
    cap = cv2.VideoCapture(0)

    ultimo_codigo = None
    ultimo_tiempo_lectura = 0

    try:
        while True:
            ret, frame = cap.read()
            if ret:
                ultimo_codigo, ultimo_tiempo_lectura = leer_codigo_qr(frame, ultimo_codigo, ultimo_tiempo_lectura)
            else:
                print("No se pudo obtener el frame de la cámara.")
                break

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    except KeyboardInterrupt:
        print("Detenido por el usuario")

    finally:
        cap.release()
        cv2.destroyAllWindows()
        arduino.close()  # Cerrar la conexión serial con Arduino

if __name__ == "__main__":
    main()
